<?php require 'header.php'; ?>
<br><br><br>
<main>
    <div class="container">
        <div class="row py-4">
            <div class="col-md-12">
                <!-- Card -->
                <div class="card card-cascade wider">
                    <!-- Card image -->
                    <div class="view view-cascade gradient-card-header central">
                        <!-- Title -->
                        <h2 class="card-header-title mb-3">Rastreo de Sucursal</h2>
                    </div>
                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center">
                        <!-- Text -->

                        <h5 class="black-text pb-2 pt-1"><i class="fas fa-map-marker"></i> Obtener Ubicación</h5>
                        <form class="md-form">
                            <ul class="list-group list-group-horizontal-md">
                                <li class="list-group-item">
                                    <div class="form-check">
                                        <input type="radio" name="ubicacion" value="automatico"
                                               class="form-check-input radio" id="Ubicacion_Actual">
                                        <label class="form-check-label" for="Ubicacion_Actual">Ubicación Actual</label>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="form-check">
                                        <input type="radio" name="ubicacion" value="manual"
                                               class="form-check-input radio" id="Ubicacion_manual">
                                        <label class="form-check-label" for="Ubicacion_manual">Ingresar dirección
                                            manualmente</label>
                                    </div>
                                </li>
                            </ul>
                        </form>
                        <form class="md-form dirPost">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="md-form">
                                        <input type="text" id="getDir" class="form-control" name="direccion"
                                               pattern="Dirección Postal"
                                               placeholder="Ejem. Av Insurgentes 552, roma sur, CDMX">
                                        <label for="getDir">Ingresar Dirección</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" value="Buscar sucursal cercana" class="btn btn-default" id="searchDir">Buscar</button>
                                </div>
                            </div>
                            <div id="resultdos2"></div>
                        </form>
                    </div>
                    <!-- Card content -->
                </div>
                <!-- Card -->
            </div>
        </div>
        <div class="row py-6">
            <div class="col-md-12">
                <div id="direccion"></div>
                <div id="preloader"></div>
                <div class="col-1"></div>
            </div>
        </div>
    </div>
</main>
<footer class="page-footer font-small black fixed-bottom">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© <?php echo date("Y"); ?> Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> Operadora Central de Estacionamientos </a>
    </div>
    <!-- Copyright -->

</footer>
<div id="scripts">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script src="https://maps.google.com/maps?file=api&v=2&key=AIzaSyBoN6cE0y9vrkzluAmHhRZVZXTACDqT2CI"
            type="text/javascript"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/mdb.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/compiled-4.8.0.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script type="text/javascript">

        var map;
        var latitudReal;
        var gdir;
        var longitudReal;
        var geocoder = null;
        var StartPosition;
        var myArray = [];
        var distance;
        var direccion_postal;
        $(".radio").click(function () {
            var a = $(this).val();
            var b = $(this);
            navigator.geolocation.getCurrentPosition(function (location) {
                latitudReal = location.coords.latitude;
                longitudReal = location.coords.longitude;
                console.log(location.coords.latitude);
                console.log(location.coords.longitude);
                StartPosition = latitudReal + "," + longitudReal;

                if ($(b).prop('checked')) {
                    if (a == 'automatico') {
                        $(".dirPost").hide();
                        automatic();
                        $("#direccion").show();
                    } else {
                        $(".dirPost").show();
                        $("#direccion").hide();
                        $('#resultdos2').html('');
                    }
                }
            });
        });
        /*navigator.geolocation.getCurrentPosition(function (location) {
            latitudReal = location.coords.latitude;
            longitudReal = location.coords.longitude;
            console.log(location.coords.latitude);
            console.log(location.coords.longitude);
            StartPosition = latitudReal + "," + longitudReal;
        });
        $(".radio").click(function () {
            var a = $(this).val();
            if ($(this).prop('checked')) {
                if (a == 'automatico') {
                    $(".dirPost").hide();
                    automatic();
                    $("#direccion").show();
                } else {
                    $(".dirPost").show();
                    $("#direccion").hide();
                    $('#resultdos2').html('');
                }
            }
        });*/

        function automatic() {
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'address': StartPosition
            }, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    document.getElementById('direccion').innerHTML =
                        "                    <div class=\"card card-cascade narrower\">\n" +
                        "                        <div class=\"view view-cascade gradient-card-header central\">\n" +
                        "                            <h2 class=\"card-header-title\">Estacionamientos</h2>\n" +
                        "                        </div>\n" +
                        "                        <!-- Card content -->\n" +
                        "                        <div class=\"card-body card-body-cascade text-center\">" +
                        "                           <h5 class=\"black-text pb-2 pt-1\"><i class=\"fas fa-map-marker\"></i> Dirección de Origen: " + results[0].formatted_address + "</h5>" +
                        "                               <br>" +
                        "                              <div id=\"resultdos\"></div>" +
                        "                        </div>\n" +
                        "                    </div>\n";
                    direccion_postal = results[0].formatted_address;
                    $.ajax({
                        url: "include/controller/compareto.php",
                        data: {'dir': direccion_postal},
                        type: "post",
                        beforeSend: function (res) {
                            $("preloader").html('<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>');
                        },
                        success: function (response) {
                            $("#resultdos").html(response);
                            $('#preloader').html('');
                        }
                    });
                } else {
                    alert('Geocode no tuvo éxito por la siguiente razón: ' + status);
                }
            });
        }

        $("#searchDir").click(function () {
            var a = $("#getDir").val();
            if (a != "") {
                manual(a);
            } else {
                $.alert('La dirección no puede esta vacia, porfavor complete el campo e intente de nuevo.');
            }
        });

        function manual(route) {
            direccion_postal = route;
            $.ajax({
                url: "include/controller/compareto.php",
                data: {'dir': direccion_postal},
                type: "post",
                beforeSend: function (res) {
                    $("preloader").html('<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>');
                },
                success: function (response) {
                    $("#resultdos2").html(response);
                    $('#preloader').html('');
                }
            });
        }

    </script>
</div>
</body>
</html>