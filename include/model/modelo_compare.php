<?php 
require '../conx/conec.php';
class Comparetodir{
		var $conn;
		var $conexion;
		var $mensajeExito;
		var $mensajeError;
		public $mensajeErrores = '';
		public $listTable = '';
		public $tableCsv = '';

		function __construct(){
			$this->conexion = new  Conexion();
			$this->conn = $this->conexion->conectarse();
			$this->mensajeExito="Registro Exitoso";
			$this->mensajeError="Error al Registrar";
		}


		function compararDir($dir){
			$enviar = $dir;
			$dir = str_replace(" ", "%", $dir);
			$searchsql = "SELECT * FROM estacionamientos WHERE sr_dir_estacionamiento like '%mexico%'";
			$rssearch = mysqli_query($this->conn, $searchsql);
			if (mysqli_num_rows($rssearch)>0) {
					$this->listTable .= "<table class='table'>";
				while ($row = mysqli_fetch_array($rssearch)) {
					$this->listTable .= '
						<tr>
	                      <td>
	                        <p class="text-muted">Dirección: </p>
	                      </td>
	                      <td><p class="text-muted">'.$row['sr_dir_estacionamiento'].'</p></td>
	                      <td>
	                        <a href="details-and-route.php?ori='.$enviar.'&des='.$row['sr_dir_estacionamiento'].'" class="btn btn-sm btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Ver detalles" data-original-title="Remove item" data-id="'.$row['sr_id_estacionamiento'].'">Ver ruta y detalles.
	                        </a>
	                        <!--<a href="details-and-route.php?ori='.$enviar.'&des='.$row['sr_dir_estacionamiento'].'" class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Editar Empleado" data-original-title="Edit item" data-id="'.$row['sr_id_estacionamiento'].'">Ver detalles.
	                        </a>-->
	                      </td>
	                    </tr>';
				}
				$this->listTable .= "</table>";
				echo $this->listTable;
			}
		}
	}