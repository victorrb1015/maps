<?php
if (!isset($_GET['ori']) && !isset($_GET['des'])) {
    header("Location: index.php");
} else {
    require 'header.php'; ?>

    <br><br><br><br>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Card -->
                    <div class="card card-cascade narrower">
                        <!-- Card image -->
                        <div class="view view-cascade gradient-card-header central">
                            <!-- Title -->
                            <h2 class="card-header-title">Detalles y ruta</h2>
                        </div>
                        <!-- Card content -->
                        <div class="card-body card-body-cascade text-center">
                            <form action="#" name="form_ruta"
                                  onsubmit="obtenerRuta(this.desde.value, this.hasta.value); return false">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-form">
                                            <input type="text" id="desde" name="desde" class="form-control"
                                                   value="<?php echo $_GET['ori'] ?>">
                                            <label for="desde">Desde:</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form">
                                            <input type="text" id="hasta" name="hasta" class="form-control"
                                                   value="<?php echo $_GET['des'] ?>">
                                            <label for="hasta">Hasta:</label>
                                        </div>
                                    </div>
                                </div>
                                <h4>Tipo de Trayecto:</h4>
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-check">
                                            <input type="radio" name="tipo"
                                                   class="form-check-input radio" id="Ubicacion_Actual" value="2" checked>
                                            <label class="form-check-label" for="Ubicacion_Actual">Coche</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="form-check">
                                            <input type="radio" name="tipo"
                                                   class="form-check-input radio" id="tipo" value="1">
                                            <label class="form-check-label" for="tipo">A pie</label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-outline-default btn-rounded waves-effect btn-block">Obtener ruta</button>
                                </div>
                            </form>
                            <hr>
                            <div><b>Distancia: </b><span id="getDistance"></span></div>
                        </div>
                    </div>
                    <!-- Card -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="row py-4">
                        <!--Card-->
                        <div class="card card-cascade narrower col-md-12">
                            <!--Card image-->
                            <div class="view view-cascade gradient-card-header central">
                                <h5 class="mb-0">Mapa</h5>
                            </div>
                            <!--/Card image-->
                            <!--Card content-->
                            <div class="card-body card-body-cascade text-center">
                                <!--Google map-->
                                <div id="map"></div>
                            </div>
                            <!--/.Card content-->
                        </div>
                        <!--/.Card-->
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row py-4 mx-2">
                        <!--Card-->
                        <div class="card card-cascade narrower col-md-12">
                            <!--Card image-->
                            <div class="view view-cascade gradient-card-header central">
                                <h5 class="mb-0">Indicaciones</h5>
                            </div>
                            <!--/Card image-->
                            <!--Card content-->
                            <div class="card-body card-body-cascade text-center tamano scrollbar-dusty-grass">
                                <div id="direcciones"></div>
                            </div>
                            <!--/.Card content-->
                        </div>
                        <!--/.Card-->
                    </div>
                </div>

            </div>
        </div>
    </main>
    <footer class="page-footer font-small black">

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© <?php echo date("Y"); ?> Copyright:
            <a href="https://mdbootstrap.com/education/bootstrap/"> Operadora Central de Estacionamientos </a>
        </div>
        <!-- Copyright -->

    </footer>
    <div id="scripts">
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script src="https://maps.google.com/maps?file=api&v=2&key=AIzaSyBoN6cE0y9vrkzluAmHhRZVZXTACDqT2CI"
                type="text/javascript"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/mdb.min.js"></script>
        <script type="text/javascript" src="js/compiled-4.8.0.min.js"></script>
        <script type="text/javascript">
            var map;
            var gdir;
            var geocoder = null;

            function load() {
                if (GBrowserIsCompatible()) {
                    map = new GMap2(document.getElementById("map"));
                    gdir = new GDirections(map, document.getElementById("direcciones"));
                    GEvent.addListener(gdir, "load", onGDirectionsLoad);
                    GEvent.addListener(gdir, "error", mostrarError);
                    obtenerRuta("<?php echo $_GET['ori'] ?>", "<?php echo $_GET['des'] ?>");
                }
            }

            function obtenerRuta(desde, hasta) {
                var i;
                var tipo;
                //comprobar tipo trayecto seleccionado
                for (i = 0; i < document.form_ruta.tipo.length; i++) {
                    if (document.form_ruta.tipo[i].checked) {
                        break;
                    }
                }
                tipo = document.form_ruta.tipo[i].value;
                if (tipo == 1) {
                    //a pie
                    gdir.load("from: " + desde + " to: " + hasta,
                        {"locale": "es", "travelMode": G_TRAVEL_MODE_WALKING});
                } else {
                    //conduccion
                    gdir.load("from: " + desde + " to: " + hasta,
                        {"locale": "es", "travelMode": G_TRAVEL_MODE_DRIVING});
                }
            }

            function onGDirectionsLoad() {
                //resumen de tiempo y distancia
                var distance = gdir.getDistance();
                document.getElementById("getDistance").innerHTML = "<b>DISTANCIA</b>" + gdir.getSummaryHtml();
                $("#getDistance").text(distance['html']);
            }

            function mostrarError() {
                if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
                    alert("No se ha encontrado una ubicación geográfica que se corresponda con la dirección especificada.");
                else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
                    alert("No se ha podido procesar correctamente la solicitud de ruta o de códigos geográficos, sin saberse el motivo exacto del fallo.");
                else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
                    alert("Falta el parámetro HTTP q o no tiene valor alguno. En las solicitudes de códigos geográficos, esto significa que se ha especificado una dirección vacía.");
                else if (gdir.getStatus().code == G_GEO_BAD_KEY)
                    alert("La clave proporcionada no es válida o no coincide con el dominio para el cual se ha indicado.");
                else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
                    alert("No se ha podido analizar correctamente la solicitud de ruta.");
                else alert("Error desconocido.");

            }
        </script>
        <script>
            window.onload = load;
            window.onunload = GUnload;
        </script>
    </div>
    </body>
    </html>
    <?php
}
?>